/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hadoop1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 *
 * @author Matt
 */
public class HadoopService extends Configured implements Tool {

   //mvn clean package antrun:run@deploy
    public static int runJob(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new HadoopService(), args);
        //System.out.println("result "+res);
        //System.exit(res);
        return res;
    }

    public static String getResult() throws IOException {
        HadoopService hs = new HadoopService();
        return hs.previewResult();
    }

    public String previewResult() throws IOException {
        Configuration conf = this.prepareConfiguration(null);
        FileSystem fs = FileSystem.get(conf);
        FSDataInputStream open = fs.open(new Path("output/part-r-00000"));
        BufferedReader br = new BufferedReader(new InputStreamReader(open));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            sb.append("\n");
            line = br.readLine();
        }
        return sb.toString();
    }

    public Configuration prepareConfiguration(Configuration conf) {
        if(conf == null)
            conf = new Configuration();
        System.setProperty("HADOOP_USER_NAME", "vagrant");
        conf.set("mapred.job.tracker", "192.168.5.10:8021");
        conf.set("fs.default.name", "hdfs://192.168.5.10:9000/user/vagrant");
        conf.set("hadoop.job.ugi", "vagrant");

        return conf;
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = this.getConf();
        conf = this.prepareConfiguration(conf);
        FileSystem fs = FileSystem.get(conf);
        if (fs.exists(new Path("output"))) {
            fs.delete(new Path("output"), true);
        }

        System.out.println("po ustawieniu plikow konfig "+ conf.toString());
        
        Job job = Job.getInstance(conf, "calculatePasswordEntrophy");
        job.setJarByClass(HadoopService.class);
        job.setMapperClass(CalculateEntrophyMapper.class);
        job.setReducerClass(PasswordReducer.class);
        job.setSortComparatorClass(SortEntrophy.class);
        job.setOutputKeyClass(DoubleWritable.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("input"));
        FileOutputFormat.setOutputPath(job, new Path("output"));
        
         //System.out.println("przed wyjsciem z run "+ job.toString());
         
        return job.waitForCompletion(true) ? 0 : 1;
    }

}
