/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hadoop1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ubuntu
 */
public class RunJobServlet extends HttpServlet {


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int result = HadoopService.runJob( new String[0]);
            response.getWriter().println("rezultat wykonania joba : " + result);
            
            if(result == 0)
            {
                response.getWriter().println("Job wykonal sie bez bledu");
            }
            else
            {
                response.getWriter().println("Job napotkal bledy : status 1");
            }    
          
            
        } catch (Exception ex) {
            response.getWriter().print("Blad :(" + ex.toString());
            Logger.getLogger(RunJobServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.getWriter().println("Program wykonany bez bloku catch - nie bylo zadnego wyjatku :)");
    }


}
